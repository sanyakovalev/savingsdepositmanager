This is the test application. 

Provided feature:
- OAuth authenticateion
- Single Page Application
- AngularJS
- User Role Manager
- WebAPI 
- Bootstrap

Formal Technical Requirements:

Write a simple savings deposit manager
* User must be able to create an account and log in. (If a mobile application, this means that more users can use the app from the same phone).
*  When logged in, a user can see, edit and delete saving deposits he entered.
*  Implement at least three roles with different permission levels: a regular user would only be able to CRUD on their owned records, a user manager would be able to CRUD users, and an admin would be able to CRUD all records and users.
*  A saving deposit is identified by a bank name, account number, initial amount saved (currency in USD), start date, end date, interest percentage per year and taxes percentage.
*  The interest could be positive or negative. The taxes are only applied over profit.
*  User can filter saving deposits by amount (minimum and maximum), bank name and date.
*  User can generate a revenue report for a given period, showing the gains and losses from interest and taxes for each deposit. The amount should be green or red if respectively it represents a gain or loss. The report should show the sum of profits and losses at the bottom for that period.
*  The computation of profit/loss is done daily basis. Consider that a year is 360 days.
*  Minimal UI/UX design.
*  I need every client operation done using JavaScript, reloading the page is not an option.
*  REST API. Make it possible to perform all user actions via the API, including authentication 
*  All actions need to be done client side using AJAX, refreshing the page is not acceptable.
*  Unit and e2e tests!