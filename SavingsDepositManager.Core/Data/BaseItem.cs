﻿using System;

namespace SavingsDepositManager.Core.Data
{
    public class BaseItem
    {
        #region Properties

        public string CreatedByName { get; set; }
        public string CreatedById { get; set; }
        public DateTime Created { get; set; }

        public string ModifiedByName { get; set; }
        public string ModifiedById { get; set; }
        public DateTime Modified { get; set; }

        #endregion

        #region .ctor

        public BaseItem()
        {
        }

        #endregion
    }
}