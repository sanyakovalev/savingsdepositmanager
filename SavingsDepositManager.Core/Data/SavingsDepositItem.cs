﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace SavingsDepositManager.Core.Data
{
    [Table("SavingsDeposits")]
    public class SavingsDepositItem : BaseItem
    {
        #region Const

        const int CompoundFrequency = 360;

        #endregion

        #region Properties

        public long Id { get; set; }
        public string BankName { get; set; }
        public string AccountNumber { get; set; }
        public decimal InitialAmount { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int InterestPercentage { get; set; }
        public int TaxesPercentage { get; set; }

        [NotMapped]
        public double InvestedYears
        {
            get
            {
                return ((EndDate - StartDate).TotalDays + 1.0) / CompoundFrequency;
            }
        }

        [NotMapped]
        public decimal Amount
        {
            get
            {
                double p = (double)InitialAmount;
                double r = InterestPercentage / 100.0;

                return (decimal)(p * Math.Pow(1 + (r / CompoundFrequency), CompoundFrequency * InvestedYears));
            }
        }

        [NotMapped]
        public decimal AmountWithTaxes
        {
            get
            {
                var amount = Amount;
                var profit = Amount - InitialAmount;

                if (profit > 0)
                {
                    var profitWithTaxes = (decimal)(TaxesPercentage / 100.0) * profit;
                    return amount - profitWithTaxes;
                }

                return amount;
            }
        }

        [NotMapped]
        public decimal ReportAmount { get; set; }
        [NotMapped]
        public decimal ReportAmountWithTaxes { get; set; }
        [NotMapped]
        public bool ReportIsGain { get; set; }

        #endregion

        #region .ctor

        public SavingsDepositItem()
        {
        }

        #endregion
    }
}