namespace SavingsDepositManager.Core.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SavingsDeposits",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        BankName = c.String(),
                        AccountNumber = c.String(),
                        InitialAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        InterestPercentage = c.Int(nullable: false),
                        TaxesPercentage = c.Int(nullable: false),
                        CreatedByName = c.String(),
                        CreatedById = c.String(),
                        Created = c.DateTime(nullable: false),
                        ModifiedByName = c.String(),
                        ModifiedById = c.String(),
                        Modified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.SavingsDeposits");
        }
    }
}