﻿using SavingsDepositManager.Core.Data;
using System;
using System.Data.Entity;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SavingsDepositManager.Core
{
    public class SavingsDepositContext : DbContext
    {
        #region Properties

        public DbSet<SavingsDepositItem> SavingsDeposits { get; set; }

        #endregion

        #region .ctor

        public SavingsDepositContext() : base("DefaultConnection")
        {
        }

        #endregion

        #region Methods

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

        public override Task<int> SaveChangesAsync()
        {
            AddTimestamps();
            return base.SaveChangesAsync();
        }

        public override int SaveChanges()
        {
            AddTimestamps();
            return base.SaveChanges();
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken)
        {
            AddTimestamps();
            return base.SaveChangesAsync(cancellationToken);
        }

        private void AddTimestamps()
        {
            var entities = ChangeTracker.Entries().Where(x => x.Entity is BaseItem && (x.State == EntityState.Added || x.State == EntityState.Modified));

            foreach (var entity in entities)
            {
                if (entity.State == EntityState.Added)
                {
                    ((BaseItem)entity.Entity).Created = DateTime.UtcNow;
                }

                ((BaseItem)entity.Entity).Modified = DateTime.UtcNow;
            }
        }

        #endregion
    }
}