﻿describe('Savings Deposit Manager', function () {
    var correctUserName = 'test123@test123.com';
    var isRegistered = false;

    function logTextElement(el) {
        el.getText().then(function (text) {
            console.log(text);
        });
    }

    var hasClass = function (element, cls) {
        return element.getAttribute('class').then(function (classes) {
            return classes.split(' ').indexOf(cls) !== -1;
        });
    };

    it('should register new user', function () {
        browser.get('https://localhost:6465/login');

        element(by.model('loginData.username')).sendKeys(correctUserName);
        element(by.model('loginData.password')).sendKeys('!2345Qwert');

        var loginButton = element(by.id('login'));
        var loginDisabled = loginButton.getAttribute('disabled');
        expect(loginDisabled).toEqual(null);

        loginButton.click();
        browser.waitForAngular();

        if (browser.driver.getCurrentUrl() == 'https://localhost:6465/login') {

            browser.get('https://localhost:6465/register');

            element(by.model('registerData.Email')).sendKeys('test123@');
            element(by.model('registerData.Password')).sendKeys('!2345Qwert');
            element(by.model('registerData.ConfirmPassword')).sendKeys('!2345Qwert');

            var registerButton = element(by.id('register'));
            var rbDisabled = registerButton.getAttribute('disabled');
            expect(rbDisabled).toEqual('true');

            element(by.model('registerData.Email')).clear();
            element(by.model('registerData.Email')).sendKeys(correctUserName);

            registerButton = element(by.id('register'));
            rbDisabled = registerButton.getAttribute('disabled');
            expect(rbDisabled).toEqual(null);

            registerButton.click();

            browser.waitForAngular();

            isRegistered = true;
        }
    });

    it('should show empty table', function () {
        browser.get('https://localhost:6465/managedeposits');

        var table = element(by.id('dataTable'));
        //logTextElement(table);
        var rows = table.all(by.tagName('tr'));
        logTextElement(rows);

        if (isRegistered) {
            expect(rows.count()).toEqual(2); // only header element and internal row for angular
        }
        else {
            expect(rows.count()).toBeGreaterThan(1); // 2 or more rows if user was registered before
        }
    });

    it('should add new item', function () {
        browser.get('https://localhost:6465/managedeposits');

        var addNew = element(by.id('addnew'));
        addNew.click().then(function () {
            browser.wait(element(by.id('addSavingsDeposit')).isDisplayed, 5000);
        }).then(function () {
            element(by.model('dataContent.BankName')).sendKeys('Super Cool Bank');
            element(by.model('dataContent.AccountNumber')).sendKeys('123454321');
            element(by.model('dataContent.InitialAmount')).clear();
            element(by.model('dataContent.InitialAmount')).sendKeys(15000);

            element(by.model('dataContent.StartDate')).clear();
            element(by.model('dataContent.StartDate')).sendKeys('2017-06-01');

            element(by.model('dataContent.EndDate')).clear();
            element(by.model('dataContent.EndDate')).sendKeys('2018-06-01');

            element(by.model('dataContent.InterestPercentage')).clear();
            element(by.model('dataContent.InterestPercentage')).sendKeys(2);

            element(by.model('dataContent.TaxesPercentage')).clear();
            element(by.model('dataContent.TaxesPercentage')).sendKeys(14);

            var saveBtn = element(by.id('save'));
            var saveBtnAttr = saveBtn.getAttribute('disabled');
            expect(saveBtnAttr).toEqual(null);

            saveBtn.click();

            browser.waitForAngular();
        });
    });

    it('should check create item validators', function () {
        browser.get('https://localhost:6465/managedeposits');

        var addNew = element(by.id('addnew'));
        addNew.click().then(function () {
            browser.wait(element(by.id('addSavingsDeposit')).isDisplayed, 5000);
        }).then(function () {
            element(by.model('dataContent.BankName')).sendKeys('Super Cool Bank');
            element(by.model('dataContent.BankName')).clear();
            expect(hasClass(element(by.id('divBankName')), 'has-error')).toBe(true);


            element(by.model('dataContent.AccountNumber')).sendKeys('123454321');
            element(by.model('dataContent.AccountNumber')).clear();
            expect(hasClass(element(by.id('divAccountNumber')), 'has-error')).toBe(true);


            element(by.model('dataContent.InitialAmount')).clear();
            expect(hasClass(element(by.id('divInitialAmount')), 'has-error')).toBe(true);


            element(by.model('dataContent.StartDate')).clear();
            expect(hasClass(element(by.id('divStartDate')), 'has-error')).toBe(true);


            element(by.model('dataContent.EndDate')).clear();
            expect(hasClass(element(by.id('divEndDate')), 'has-error')).toBe(true);


            element(by.model('dataContent.InterestPercentage')).clear();
            expect(hasClass(element(by.id('divInterestPercentage')), 'has-error')).toBe(true);


            element(by.model('dataContent.TaxesPercentage')).clear();
            expect(hasClass(element(by.id('divTaxesPercentage')), 'has-error')).toBe(true);


            var closeBtn = element(by.id('close'));
            closeBtn.click();

            browser.waitForAngular();
        });
    });
});
