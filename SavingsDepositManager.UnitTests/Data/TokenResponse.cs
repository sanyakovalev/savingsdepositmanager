﻿using Newtonsoft.Json;
using System;

namespace SavingsDepositManager.UnitTests.Data
{
    public class TokenResponse
    {
        #region Properties

        [JsonProperty("access_token")]
        public string AccessToken { get; set; }

        [JsonProperty("token_type")]
        public string TokenType { get; set; }

        [JsonProperty("expires_in")]
        public long ExpiresIn { get; set; }

        [JsonProperty("userName")]
        public string UserName { get; set; }

        [JsonProperty("roles")]
        public string Roles { get; set; }

        [JsonProperty(".issued")]
        public DateTime Issued{ get; set; }

        [JsonProperty(".expires")]
        public DateTime Expires { get; set; }

        #endregion

        #region .ctor

        public TokenResponse()
        {
        }

        #endregion
    }
}