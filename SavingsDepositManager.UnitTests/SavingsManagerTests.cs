﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SavingsDepositManager.Controllers.API;
using SavingsDepositManager.Core.Data;
using SavingsDepositManager.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Results;

namespace SavingsDepositManager.UnitTests
{
    /// <summary>
    /// Used article http://blogs.msmvps.com/theproblemsolver/2013/11/13/unit-testing-a-asp-net-webapi-2-controller/
    /// </summary>
    [TestClass]
    public class SavingsManagerTests
    {
        /// <summary>
        /// Testing Get All Saving deposits
        /// </summary>
        [TestMethod]
        public void WhenGettingItShouldReturnAllItems()
        {
            // Arrange
            var controller = new SavingsDepositItemsController(true);

            // Act
            var actionResult = controller.GetSavingsDeposits(null, 0, 0, null, null, 10, 0, false);

            // Assert
            var response = actionResult as OkNegotiatedContentResult<SavingsDepositResultModel>;
            Assert.IsNotNull(response);
            Assert.IsNotNull(response.Content);
            Assert.IsNotNull(response.Content.Items);
            Assert.IsTrue(response.Content.Items.Count() <= 10);
        }

        /// <summary>
        /// Testing Get report
        /// </summary>
        [TestMethod]
        public void WhenGettingReportItShouldReturnReport()
        {
            // Arrange
            var controller = new SavingsDepositItemsController(true);

            // Act
            var actionResult = controller.GetSavingsDepositsReport(DateTime.Now.AddMonths(-12), DateTime.Now.AddMonths(12));

            // Assert
            var response = actionResult as OkNegotiatedContentResult<SavingsDepositResultModel>;
            Assert.IsNotNull(response);
            Assert.IsNotNull(response.Content);
            Assert.IsNotNull(response.Content.Items);
        }

        /// <summary>
        /// Testing one saving deposit with known item id
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task WhenGettingWithAKnownIdItShouldReturnThatItem()
        {
            // Arrange 
            var controller = new SavingsDepositItemsController(true);

            var allItems = controller.GetSavingsDeposits(null, 0, 0, null, null, 10, 0, false);
            var allItemsResponse = allItems as OkNegotiatedContentResult<SavingsDepositResultModel>;
            if (allItemsResponse.Content.Items.Count() > 0)
            {
                var itemId = allItemsResponse.Content.Items.First().Id;
                // Act
                var actionResult = await controller.GetSavingsDepositItem(itemId);

                // Assert
                var response = actionResult as OkNegotiatedContentResult<SavingsDepositItem>;
                Assert.IsNotNull(response);
                Assert.AreEqual(itemId, response.Content.Id);
            }
        }

        /// <summary>
        /// Testing get saving deposit with unknown id
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task WhenGettingWithAnUnknownIdItShouldReturnNotFound()
        {
            // Arrange
            var controller = new SavingsDepositItemsController(true);

            // Act
            var actionResult = await controller.GetSavingsDepositItem(999);

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(NotFoundResult));
        }

        /// <summary>
        /// Testing creating new item
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task WhenPostingANewItemShouldBeAdded()
        {
            // Arrange
            var controller = new SavingsDepositItemsController(true);

            // Act
            var actionResult = await controller.PostSavingsDepositItem(new SavingsDepositItem()
            {
                AccountNumber = "TestAcc",
                BankName = "Test Bank Name",
                StartDate = DateTime.Today,
                EndDate = DateTime.Today.AddMonths(24),
                InitialAmount = 5000,
                InterestPercentage = 5,
                TaxesPercentage = 13
            });

            // Assert
            var response = actionResult as CreatedAtRouteNegotiatedContentResult<SavingsDepositItem>;

            Assert.IsNotNull(response);
            Assert.AreEqual("DefaultApi", response.RouteName);
            Assert.AreEqual(response.Content.Id, response.RouteValues["Id"]);
        }

        /// <summary>
        /// Test update method
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task WhenPuttingAItemItShouldBeUpdated()
        {
            // Arrange
            var controller = new SavingsDepositItemsController(true);

            var allItems = controller.GetSavingsDeposits(null, 0, 0, null, null, 10, 0, false);
            var allItemsResponse = allItems as OkNegotiatedContentResult<SavingsDepositResultModel>;
            if (allItemsResponse.Content.Items.Count() > 0)
            {
                var item = allItemsResponse.Content.Items.First();

                item.BankName += "Upd";
                var actionResult = await controller.PutSavingsDepositItem(item.Id, item);

                // Assert
                var response = actionResult as CreatedAtRouteNegotiatedContentResult<SavingsDepositItem>;

                Assert.IsNotNull(response);
                Assert.AreEqual("DefaultApi", response.RouteName);
                Assert.AreEqual(response.Content.Id, response.RouteValues["Id"]);
                Assert.AreEqual(item.BankName, response.Content.BankName);
            }            
        }

        /// <summary>
        /// Test delete method
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task WhenDeletingAItemItShouldBeDeleted()
        {
            // Arrange
            var controller = new SavingsDepositItemsController(true);

            var allItems = controller.GetSavingsDeposits(null, 0, 0, null, null, 10, 0, false);
            var allItemsResponse = allItems as OkNegotiatedContentResult<SavingsDepositResultModel>;
            if (allItemsResponse.Content.Items.Count() > 0)
            {
                var itemId = allItemsResponse.Content.Items.First().Id;
                // Act
                var actionResult = await controller.DeleteSavingsDepositItem(itemId);

                // Assert
                var response = actionResult as OkNegotiatedContentResult<SavingsDepositItem>;
                Assert.IsNotNull(response);
                Assert.AreEqual(itemId, response.Content.Id);
            }
        }

        /// <summary>
        /// Test delete method with wrong id 
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task WhenDeletingWithWrongIdItShouldNotFound()
        {
            // Arrange
            var controller = new SavingsDepositItemsController(true);

            // Act
            var actionResult = await controller.DeleteSavingsDepositItem(999);

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(NotFoundResult));
        }
    }
}