﻿using Microsoft.Owin.Testing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using SavingsDepositManager.UnitTests.Data;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace SavingsDepositManager.UnitTests
{
    [TestClass]
    public class UsersTests
    {
        public static TokenResponse Token { get; set; }
        public static string NewUserName { get; set; }

        /// <summary>
        /// Login with Admin credentials. This user created by default when DB context first time initialized
        /// </summary>
        [TestMethod]
        public async Task LoginWithAdminCred_ShouldBeOk()
        {
            using (var server = TestServer.Create<StartupTest>())
            {
                var response = await server.CreateRequest("/Token").And(x => x.Content = new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("username", "admin@admin.com"),
                    new KeyValuePair<string, string>("password", "12345qweRT!@"),
                    new KeyValuePair<string, string>("grant_type", "password")
                })).PostAsync();

                var isSuccessStatusCode = response.IsSuccessStatusCode;
                Assert.AreEqual(true, isSuccessStatusCode);

                var s = await response.Content.ReadAsStringAsync();
                Token = JsonConvert.DeserializeObject<TokenResponse>(s);

                Assert.IsNotNull(Token);
                Assert.IsNotNull(Token.AccessToken);
            }
        }

        /// <summary>
        /// Login with wrond credentials
        /// </summary>
        [TestMethod]
        public async Task LoginWithWrongCred_ShouldBeError()
        {
            using (var server = TestServer.Create<StartupTest>())
            {
                var response = await server.CreateRequest("/Token").And(x => x.Content = new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("username", "admin@admin.com"),
                    new KeyValuePair<string, string>("password", "12345"),
                    new KeyValuePair<string, string>("grant_type", "password")
                })).PostAsync();

                var isSuccessStatusCode = response.IsSuccessStatusCode;
                Assert.AreEqual(false, isSuccessStatusCode);

                Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            }
        }

        /// <summary>
        /// Register new user with random generated name
        /// </summary>
        [TestMethod]
        public async Task RegisterNewUser_ShouldBeOK()
        {
            using (var server = TestServer.Create<StartupTest>())
            {
                var rnd = new Random((int)DateTime.Now.Ticks);
                var num = rnd.Next(1000, 9999);
                NewUserName = $"test{num}@test{num}.com";

                var response = await server.CreateRequest("/api/Account/Register").And(x => x.Content = new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("Email", NewUserName),
                    new KeyValuePair<string, string>("Password", "!2345Qwert"),
                    new KeyValuePair<string, string>("ConfirmPassword", "!2345Qwert")
                })).PostAsync();

                var isSuccessStatusCode = response.IsSuccessStatusCode;
                Assert.AreEqual(true, isSuccessStatusCode);

                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            }
        }

        /// <summary>
        /// Login with user created from <see cref="RegisterNewUser_ShouldBeOK"/>
        /// </summary>
        [TestMethod]
        public async Task LoginWithNewUser_ShouldBeOK()
        {
            using (var server = TestServer.Create<StartupTest>())
            {
                var response = await server.CreateRequest("/Token").And(x => x.Content = new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("username", NewUserName),
                    new KeyValuePair<string, string>("password", "!2345Qwert"),
                    new KeyValuePair<string, string>("grant_type", "password")
                })).PostAsync();

                var isSuccessStatusCode = response.IsSuccessStatusCode;
                Assert.AreEqual(true, isSuccessStatusCode);

                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

                var s = await response.Content.ReadAsStringAsync();
                Token = JsonConvert.DeserializeObject<TokenResponse>(s);

                Assert.IsNotNull(Token);
                Assert.IsNotNull(Token.AccessToken);
            }
        }

        /// <summary>
        /// Trying to register the same user as we registered in <see cref="RegisterNewUser_ShouldBeOK"/>
        /// </summary>
        [TestMethod]
        public async Task RegisterTheSameUser_ShouldBeError()
        {
            using (var server = TestServer.Create<StartupTest>())
            {
                var response = await server.CreateRequest("/api/Account/Register").And(x => x.Content = new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("Email", NewUserName),
                    new KeyValuePair<string, string>("Password", "Qwert!2345"),
                    new KeyValuePair<string, string>("ConfirmPassword", "Qwert!2345")
                })).PostAsync();

                var isSuccessStatusCode = response.IsSuccessStatusCode;
                Assert.AreEqual(false, isSuccessStatusCode);

                Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            }
        }

        /// <summary>
        /// Log out
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task LogoutNewUser_ShouldBeOk()
        {
            using (var server = TestServer.Create<StartupTest>())
            {
                var response = await server.CreateRequest("/api/Account/Logout")
                    .AddHeader("Authorization", "Bearer " + Token.AccessToken)
                    .PostAsync();

                var isSuccessStatusCode = response.IsSuccessStatusCode;
                Assert.AreEqual(true, isSuccessStatusCode);

                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

                Token = null;
            }
        }
    }
}