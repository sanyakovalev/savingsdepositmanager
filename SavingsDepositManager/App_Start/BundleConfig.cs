﻿using System.Web.Optimization;

namespace SavingsDepositManager
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/angular").Include(
                        "~/Scripts/angular.js",
                        "~/Scripts/angular-animate.js",
                        "~/Scripts/angular-mocks.js",
                        "~/Scripts/angular-resource.js",
                        "~/Scripts/angular-route.js",
                        "~/Scripts/angular-ui-router.js",
                        "~/Scripts/angular-ui/ui-bootstrap-tpls.js",
                        "~/Scripts/ng-table.js",
                        "~/Scripts/angular.rangeSlider.js",
                        "~/Scripts/angular-block-ui.js",
                        "~/Scripts/ngStorage.js",
                        "~/Scripts/notify/angular-notify.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/myControllers").Include(
                "~/Scripts/Filters/filters.js",
                "~/Scripts/Directives/directives.js",
                "~/Scripts/Factories/factories.js",
                "~/Scripts/Controllers/savingsdepositmanager.js",
                "~/Scripts/Controllers/manageusers.js",
                "~/Scripts/Controllers/report.js",
                "~/Scripts/Controllers/site.js"
            ));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js",
                      "~/Scripts/bootstrap-slider.min.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css",
                      "~/Content/ng-table.css",
                      "~/Content/angular.rangeSlider.css",
                      "~/Content/angular-block-ui.css",
                      "~/Content/ui-bootstrap-csp.css",
                      "~/Content/notify/angular-notify.css"));
        }
    }
}
