﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SavingsDepositManager.Models;
using SavingsDepositManager.Models.Users;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

namespace SavingsDepositManager.Controllers.API
{
    [Authorize(Roles = "Admin,Manager")]
    public class ManageUsersController : ApiController
    {
        #region Fields

        private ApplicationDbContext db = new ApplicationDbContext();
        private ApplicationUserManager _userManager;

        #endregion

        #region Properties

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        #endregion

        #region Methods

        public IHttpActionResult GetUsers(int? take, int? skip)
        {
            var currentUserId = User.Identity.GetUserId();

            var adminRoleId = db.Roles.Where(r => r.Name == "Admin").Select(r => r.Id).FirstOrDefault();
            var managerRoleId = db.Roles.Where(r => r.Name == "Manager").Select(r => r.Id).FirstOrDefault();

            var result = new ManageUsersResultModel();
            var appUsers = db.Users.AsNoTracking().AsQueryable();
            result.Total = appUsers.Count();

            if (take.HasValue && take.Value > 0)
            {
                if (skip.HasValue && skip.Value > 0)
                {
                    appUsers = appUsers.OrderBy(i => i.Email).Skip(skip.Value).Take(take.Value);
                }
                else
                {
                    appUsers = appUsers.OrderBy(i => i.Email).Take(take.Value);
                }
            }

            var resultUsers = new List<UserInfo>();
            foreach (var appUser in appUsers.ToList())
            {
                var userInfo = new UserInfo();
                userInfo.Email = appUser.Email;
                userInfo.Id = appUser.Id;

                userInfo.IsAdmin = appUser.Roles.Where(r => r.RoleId == adminRoleId).Count() > 0;
                userInfo.IsManager = appUser.Roles.Where(r => r.RoleId == managerRoleId).Count() > 0;
                userInfo.IsCurrentUser = appUser.Id == currentUserId;
            
                resultUsers.Add(userInfo);
            }

            result.Items = resultUsers.AsQueryable();
            return Ok(result);
        }

        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutUser(string id, UserInfo userItem)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != userItem.Id)
            {
                return BadRequest();
            }

            var appUser = await UserManager.FindByIdAsync(id);

            var userRoleId = db.Roles.Where(r => r.Name == "User").Select(r => r.Id).FirstOrDefault();
            if (appUser.Roles.Where(r => r.RoleId == userRoleId).Count() == 0)
            {
                await UserManager.AddToRoleAsync(appUser.Id, "User");
            }

            if (userItem.IsAdmin) await UserManager.AddToRoleAsync(appUser.Id, "Admin");
            else await UserManager.RemoveFromRoleAsync(appUser.Id, "Admin");

            if (userItem.IsManager) await UserManager.AddToRoleAsync(appUser.Id, "Manager");
            else await UserManager.RemoveFromRoleAsync(appUser.Id, "Manager");

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserItemExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = userItem.Id }, userItem);
        }

        // DELETE: api/ManageUsers/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> DeleteUserItem(string id)
        {
            var appUser = await UserManager.FindByIdAsync(id);
            if (appUser == null)
            {
                return NotFound();
            }

            await UserManager.DeleteAsync(appUser);

            return StatusCode(HttpStatusCode.NoContent);
        }

        private bool UserItemExists(string id)
        {
            return db.Users.Count(e => e.Id == id) > 0;
        }

        #endregion
    }
}