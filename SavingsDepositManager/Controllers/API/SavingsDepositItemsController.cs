﻿using Microsoft.AspNet.Identity;
using SavingsDepositManager.Core;
using SavingsDepositManager.Core.Data;
using SavingsDepositManager.Models;
using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace SavingsDepositManager.Controllers.API
{
    [Authorize]
    public class SavingsDepositItemsController : ApiController
    {
        #region Fields

        private SavingsDepositContext db = new SavingsDepositContext();
        private bool _isTestMode = false;

        #endregion

        #region .ctors

        public SavingsDepositItemsController()
        {
        }
        public SavingsDepositItemsController(bool isTestMode = false)
        {
            _isTestMode = isTestMode;
        }

        #endregion

        #region Methods

        // GET: api/SavingsDepositItems
        public IHttpActionResult GetSavingsDeposits(string bankName, decimal amountMin, decimal amountMax, DateTime? startDate, DateTime? endDate,  int? take, int? skip, bool calculateMax)
        {
            var result = new SavingsDepositResultModel();
            result.Items = db.SavingsDeposits.AsNoTracking().AsQueryable();

            // Check role. If admin or test mode(unit tests) - return all items, if not - return only items for current user
            if (!User.IsInRole("Admin") || _isTestMode)
            {
                var userId = User.Identity.GetUserId();
                result.Items = result.Items.Where(r => r.CreatedById == userId);
            }

            if (calculateMax) // it means for first requrest we need to calculate max value through all items 
                // also, after add/remove/update 
            {
                var items = result.Items.ToList();
                if (items.Count > 0)
                {
                    result.MaxAmount = items.Max(r => r.Amount);
                }
            }

            #region apply filters

            if (!string.IsNullOrEmpty(bankName))
            {
                result.Items = result.Items.Where(r => r.BankName.Contains(bankName));
            }

            if (startDate.HasValue)
            {
                result.Items = result.Items.Where(r => r.StartDate >= startDate.Value);
            }

            if (endDate.HasValue)
            {
                result.Items = result.Items.Where(r => r.EndDate <= endDate.Value);
            }



            if (amountMin != 0)
            {
                var items = result.Items.ToList();
                result.Items = items.Where(r => r.Amount >= amountMin).AsQueryable();
            }

            if (amountMax != 0)
            {
                var items = result.Items.ToList();
                result.Items = items.Where(r => r.Amount <= amountMax).AsQueryable();
            }

            #endregion

            result.Total = result.Items.Count();

            #region apply paginations

            if (take.HasValue && take.Value > 0)
            {
                if (skip.HasValue && skip.Value > 0)
                {
                    result.Items = result.Items.OrderBy(i => i.Id).Skip(skip.Value).Take(take.Value);
                }
                else
                {
                    result.Items = result.Items.OrderBy(i => i.Id).Take(take.Value);
                }
            }

            #endregion

            return Ok(result);
        }

        [Route("api/SavingsDepositItems/report")]
        public IHttpActionResult GetSavingsDepositsReport(DateTime startDate, DateTime endDate)
        {
            var result = new SavingsDepositResultModel();
            result.TotalGain = 0;
            result.TotalLoss = 0;

            var items = db.SavingsDeposits.AsNoTracking().AsQueryable();

            // Check role. If admin or test mode(unit tests) - return all items, if not - return only items for current user
            if (!User.IsInRole("Admin") || _isTestMode)
            {
                var userId = User.Identity.GetUserId();
                items = items.Where(r => r.CreatedById == userId).AsQueryable();
            }

            var itemsList = items.Where(sr => sr.StartDate <= endDate && sr.EndDate >= startDate).ToList();
            
            // calculate gain or loss for specified datetime range
            foreach (var item in itemsList)
            {
                var s = startDate < item.StartDate ? item.StartDate : startDate;
                var e = endDate < item.EndDate ? endDate : item.EndDate;

                double investedYears = ((e - s).TotalDays + 1.0) / 360;
                double p = (double)item.InitialAmount;
                double r = item.InterestPercentage / 100.0;

                item.ReportAmount = item.ReportAmountWithTaxes = (decimal)(p * Math.Pow(1 + (r / 360), 360 * investedYears));
                var profit = item.ReportAmount - item.InitialAmount;

                if (profit > 0)
                {
                    var profitWithTaxes = (decimal)(item.TaxesPercentage / 100.0) * profit;
                    item.ReportAmountWithTaxes = item.ReportAmount - profitWithTaxes;
                    item.ReportIsGain = true;

                    result.TotalGain += profit;
                }
                else
                {
                    item.ReportIsGain = false;

                    result.TotalLoss += profit;
                }
            }

            result.Items = itemsList.AsQueryable();

            return Ok(result);
        }

        // GET: api/SavingsDepositItems/5
        [ResponseType(typeof(SavingsDepositItem))]
        public async Task<IHttpActionResult> GetSavingsDepositItem(long id)
        {
            SavingsDepositItem savingsDepositItem = await db.SavingsDeposits.FindAsync(id);
            if (savingsDepositItem == null)
            {
                return NotFound();
            }

            return Ok(savingsDepositItem);
        }

        // PUT: api/SavingsDepositItems/5
        [ResponseType(typeof(SavingsDepositItem))]
        public async Task<IHttpActionResult> PutSavingsDepositItem(long id, SavingsDepositItem savingsDepositItem)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != savingsDepositItem.Id)
            {
                return BadRequest();
            }

            var userId = User.Identity.GetUserId();
            if (!User.IsInRole("Admin")) // if user not admin - we need to check permissions 
            {
                // if user do not own this record - he can't update it
                if (!savingsDepositItem.CreatedById.Equals(userId, StringComparison.InvariantCultureIgnoreCase))
                {
                    return Unauthorized();
                }
            }

            savingsDepositItem.ModifiedById = userId;
            savingsDepositItem.ModifiedByName = User.Identity.GetUserName();

            db.Entry(savingsDepositItem).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SavingsDepositItemExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            //return StatusCode(HttpStatusCode.NoContent);
            return CreatedAtRoute("DefaultApi", new { id = savingsDepositItem.Id }, savingsDepositItem);
        }

        // POST: api/SavingsDepositItems
        [ResponseType(typeof(SavingsDepositItem))]
        public async Task<IHttpActionResult> PostSavingsDepositItem(SavingsDepositItem savingsDepositItem)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            savingsDepositItem.CreatedById = savingsDepositItem.ModifiedById = User.Identity.GetUserId();
            savingsDepositItem.CreatedByName = savingsDepositItem.ModifiedByName = User.Identity.GetUserName();

            db.SavingsDeposits.Add(savingsDepositItem);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = savingsDepositItem.Id }, savingsDepositItem);
        }

        // DELETE: api/SavingsDepositItems/5
        [ResponseType(typeof(SavingsDepositItem))]
        public async Task<IHttpActionResult> DeleteSavingsDepositItem(long id)
        {
            SavingsDepositItem savingsDepositItem = await db.SavingsDeposits.FindAsync(id);
            if (savingsDepositItem == null)
            {
                return NotFound();
            }

            var userId = User.Identity.GetUserId();
            if (!User.IsInRole("Admin")) // if user not admin - we need to check permissions 
            {
                // if user do not own this record - he can't delete it
                if (!savingsDepositItem.CreatedById.Equals(userId, StringComparison.InvariantCultureIgnoreCase))
                {
                    return Unauthorized();
                }
            }

            db.SavingsDeposits.Remove(savingsDepositItem);
            await db.SaveChangesAsync();

            return Ok(savingsDepositItem);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SavingsDepositItemExists(long id)
        {
            return db.SavingsDeposits.Count(e => e.Id == id) > 0;
        }

        #endregion
    }
}