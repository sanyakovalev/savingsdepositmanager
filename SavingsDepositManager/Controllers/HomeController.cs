﻿using System.Web.Mvc;

namespace SavingsDepositManager.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}