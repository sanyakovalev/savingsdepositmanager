﻿using SavingsDepositManager.Models.Users;
using System.Linq;

namespace SavingsDepositManager.Models
{
    public class ManageUsersResultModel
    {
        #region Properties

        public IQueryable<UserInfo> Items { get; set; }
        public int Total { get; set; }

        #endregion

        #region .ctor

        public ManageUsersResultModel()
        {
        }

        #endregion
    }
}