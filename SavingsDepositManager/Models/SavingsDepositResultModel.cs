﻿using SavingsDepositManager.Core.Data;
using System.Linq;

namespace SavingsDepositManager.Models
{
    public class SavingsDepositResultModel
    {
        #region Properties

        public IQueryable<SavingsDepositItem> Items { get; set; }
        public int Total { get; set; }
        public decimal MaxAmount { get; set; }

        public decimal TotalGain { get; set; }
        public decimal TotalLoss { get; set; }

        #endregion

        #region .ctor

        public SavingsDepositResultModel()
        {
        }

        #endregion
    }
}