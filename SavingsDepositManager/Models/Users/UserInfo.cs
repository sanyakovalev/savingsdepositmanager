﻿namespace SavingsDepositManager.Models.Users
{
    public class UserInfo
    {
        public string Id { get; set; }
        public string Email { get; set; }

        public bool IsAdmin { get; set; }
        public bool IsManager { get; set; }

        public bool IsCurrentUser { get; set; }

        public UserInfo()
        {
            IsAdmin = false;
            IsManager = false;
        }
    }
}