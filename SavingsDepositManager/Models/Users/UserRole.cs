﻿namespace SavingsDepositManager.Models.Users
{
    /// <summary>
    /// User role information
    /// </summary>
    public class UserRole
    {
        #region Properties

        /// <summary>
        /// User id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// User name
        /// </summary>
        public string Name { get; set; }

        #endregion
    }
}