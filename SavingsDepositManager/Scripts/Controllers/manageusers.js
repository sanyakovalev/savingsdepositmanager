﻿var manageUsersApp = angular.module('manageUsersApp', ['ngResource']);

manageUsersApp.controller('manageUsersController', function ($scope, $rootScope, $uibModal, NgTableParams, blockUI, ManageUsersFactory, notify) {

    $scope.users = [];
    $scope.tableParams = new NgTableParams(
        {
            page: 1,            // show first page
            count: 5,           // count per page
        },
        {
            total: 0,
            counts: [5, 10],
            getData: function (params) {
                blockUI.start();

                return ManageUsersFactory.getUsers({
                    take: params.count(),
                    skip: (params.page() - 1) * params.count()
                }).$promise.then(function (data) {
                    blockUI.stop();
                    params.total(data.Total); // recal. page nav controls
                    $scope.users = data.Items;

                    return data.Items;
                });
            }
        });

    $scope.reloadTable = function () {
        $scope.tableParams.page(1);
        $scope.tableParams.reload();
    }

    $scope.deleteClick = function (item, index) {
        if (confirm('Are you sure you want to delete "' + item.Email + '"?')) {
            ManageUsersFactory.delete({ id: item.Id },
                function (status) {
                    notify({
                        message: 'User successfully deleted.',
                        position: 'right',
                        classes: 'notify-success',
                        duration: 3000
                    });

                    $scope.users.splice(index, 1);
                },
                function (error) {
                    notify({
                        message: 'Some error occured in deleting user. Please try again.',
                        position: 'right',
                        classes: 'notify-danger',
                        duration: 5000
                    });
                });
        }
    }

    $scope.openUpdateModal = function (item, index) {
        $scope.modItemId = index;
        $scope.modalInstance = $uibModal.open({
            templateUrl: 'manageUserModal.html',
            controller: 'manageUsersModalController',
            //size: 'sm',
            resolve: {
                DataContent: function () {
                    var copyItem = angular.copy(item);
                    return copyItem;
                }
            }
        });

        $scope.modalInstance.result.then(function (item) {
            var gridElem = $scope.users[$scope.modItemId];
            gridElem.IsAdmin = item.IsAdmin;
            gridElem.IsManager = item.IsManager;

            notify({
                message: 'Item successfully updated.',
                position: 'right',
                classes: 'notify-success',
                duration: 3000
            });
        },
        function () {
        });
    }

    $scope.editVisibility = function (row) {
        if (row.IsCurrentUser) return false;

        if (row.IsAdmin && $rootScope.isAdmin)
            return true;

        if (!row.IsAdmin && $rootScope.isAdminOrManager)
            return true;

        return false;
    }
});

manageUsersApp.controller('manageUsersModalController', function ($scope, $rootScope, $uibModalInstance, ManageUsersFactory, DataContent, notify, $localStorage) {

    $scope.dataContent = DataContent;
    $scope.modalTitle = 'Edit User';

    $scope.closeModalClick = function () {
        $uibModalInstance.dismiss('cancel');
    }

    $scope.saveUser = function () {
        ManageUsersFactory.update({ id: $scope.dataContent.Id }, $scope.dataContent,
            function (result) {
                $uibModalInstance.close(result);
            },
            function (error) {
                notify({
                    message: 'Some error occured in updating user.',
                    position: 'right',
                    classes: 'notify-danger',
                    duration: 5000
                });
            }
        );
    }
});