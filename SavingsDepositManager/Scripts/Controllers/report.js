﻿var reportApp = angular.module('reportApp', ['ngResource']);

reportApp.controller('reportController', function ($scope, $uibModal, NgTableParams, blockUI, SavingsDepositFactory, notify) {
    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };
    $scope.endDateOptions = {
        formatYear: 'yy',
        startingDay: 1
    }
    $scope.format = 'MM/dd/yyyy';
    $scope.altInputFormats = ['M!/d!/yyyy'];

    $scope.startDateSelector = {
        opened: false
    };
    $scope.endDateSelector = {
        opened: false
    };

    $scope.filterReport = {
        startDate: new Date(new Date().getFullYear(), 0, 1),
        endDate: new Date(new Date().getFullYear(), 11, 31)
    };

    $scope.getUTCDate = function (date) {
        try {
            return new Date(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(), 0, 0, 0);
        }
        catch (err) { }
    }
    $scope.dateInvalid = false;
    $scope.validateMinDate = function () {
        if ($scope.getUTCDate($scope.filterReport.endDate) < $scope.getUTCDate($scope.filterReport.startDate)) {
            $scope.dateInvalid = true;
        }
        else {
            $scope.dateInvalid = false;
        }
    }

    $scope.totalGain = 0;
    $scope.totalLoss = 0;

    $scope.resetFilterClick = function () {
        
        $scope.filterReport.startDate = '';
        $scope.filterReport.endDate = '';

        $scope.reloadTable();
    }

    $scope.savingsDepositItems = [];
    $scope.tableParams = new NgTableParams(
        {
            page: 1,            // show first page
            count: 50000,           // count per page
        },
        {
            //total: 0,
            counts: [],
            getData: function (params) {
                blockUI.start();

                if ($scope.filterReport.startDate == undefined) $scope.filterReport.startDate = '';
                if ($scope.filterReport.endDate == undefined) $scope.filterReport.endDate = '';

                return SavingsDepositFactory.report({
                    startDate: $scope.filterReport.startDate,
                    endDate: $scope.filterReport.endDate
                }).$promise.then(function (data) {
                    blockUI.stop();
                    $scope.savingsDepositItems = data.Items;

                    $scope.totalGain = data.TotalGain
                    $scope.totalLoss = data.TotalLoss;

                    return data.Items;
                });
            }
        });

    $scope.reloadTable = function () {
        $scope.tableParams.page(1);
        $scope.tableParams.reload();
    }
});