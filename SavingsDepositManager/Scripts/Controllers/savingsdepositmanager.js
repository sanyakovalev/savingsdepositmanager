﻿var savingsDepositManagerApp = angular.module('savingsDepositManagerApp', ['ngResource']);

savingsDepositManagerApp.controller('savingsDepositManagerController', function ($scope, $uibModal, NgTableParams, blockUI, SavingsDepositFactory) {
    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };
    $scope.endDateOptions = {
        formatYear: 'yy',
        startingDay: 1
    }
    $scope.format = 'MM/dd/yyyy';
    $scope.altInputFormats = ['M!/d!/yyyy'];

    $scope.startDateSelector = {
        opened: false
    };
    $scope.endDateSelector = {
        opened: false
    };
    $scope.filter = {
        bankName: '',
        range:{
            min: 0,
            max: 0,
            maxCalculated: false
        },

        minValue: 0,
        maxValue: 0,
        startDate: '',
        endDate: ''
    };

    $scope.resetFilterClick = function () {
        $scope.filter.bankName = '';
        $scope.filter.minValue = 0;
        $scope.filter.maxValue = $scope.filter.range.max;
        $scope.filter.startDate = '';
        $scope.filter.endDate = '';

        $scope.reloadTable();
    }

    $scope.savingsDepositItems = [];
    $scope.tableParams = new NgTableParams(
        {
            page: 1,            // show first page
            count: 5,           // count per page
        },
        {
            filterSwitch: true,
            total: 0, // length of data
            counts: [5, 10, 25, 50, 100],
            getData: function (params) {
                blockUI.start();

                if ($scope.filter.startDate == undefined) $scope.filter.startDate = '';
                if ($scope.filter.endDate == undefined) $scope.filter.endDate = '';

                return SavingsDepositFactory.queryFiltered({
                    bankName: $scope.filter.bankName,
                    amountMin: $scope.filter.minValue,
                    amountMax: $scope.filter.maxValue,
                    startDate: $scope.filter.startDate,
                    endDate: $scope.filter.endDate,

                    take: params.count(),
                    skip: (params.page() - 1) * params.count(),
                    calculateMax: !$scope.filter.range.maxCalculated
                }).$promise.then(function (data) {
                    blockUI.stop();
                    params.total(data.Total);
                    $scope.savingsDepositItems = data.Items;

                    if (!$scope.filter.range.maxCalculated) {
                        $scope.filter.range.maxCalculated = true;
                        $scope.filter.range.max = data.MaxAmount;
                        $scope.filter.maxValue = data.MaxAmount;
                    }
                    return data.Items;
                });
            }
        });

    $scope.reloadTable = function () {
        $scope.tableParams.page(1);
        $scope.tableParams.reload();
    }

    $scope.deleteClick = function (item, index) {
        if (confirm('Are you sure you want to delete item "' + item.BankName + '"?')) {
            SavingsDepositFactory.delete({ id: item.Id },
                function (status) {
                    var total = $scope.tableParams.total();
                    $scope.tableParams.total(total - 1);

                    $scope.filter.range.maxCalculated = false;

                    $scope.savingsDepositItems.splice(index, 1);
                    $scope.reloadTable();
                }, function (error) {

                });
        }
    }

    $scope.openAddModal = function () {
        $scope.modalInstance = $uibModal.open({
            templateUrl: 'addSavingsDeposit.html',
            controller: 'manageSavingsDepositManagerController',
            //size: 'sm',
            resolve: {
                DataContent: function () {
                    return {
                        Id: 0,
                        BankName: '',
                        AccountNumber: '',
                        InitialAmount: 0,
                        StartDate: new Date(),
                        EndDate: new Date(),
                        InterestPercentage: 0,
                        TaxesPercentage: 0,
                        Created: (new Date()).toISOString().replace(/Z/gi, ''),
                        Modified: (new Date()).toISOString().replace(/Z/gi, ''),
                        CreatedByName: '',
                        ModifiedByName: ''
                    }
                }
            }
        });

        $scope.modalInstance.result.then(function (item) {
            $scope.filter.range.maxCalculated = false;
            $scope.savingsDepositItems.push(item);
            $scope.tableParams.total($scope.savingsDepositItems.length);
        },
        function () {
        });
    }

    $scope.openUpdateModal = function (item, index) {
        $scope.modItemId = index;
        $scope.modalInstance = $uibModal.open({
            templateUrl: 'addSavingsDeposit.html',
            controller: 'manageSavingsDepositManagerController',
            //size: 'sm',
            resolve: {
                DataContent: function () {
                    var copyItem = angular.copy(item);
                    var startDate = new Date(item.StartDate);
                    copyItem.StartDate = new Date(startDate.getUTCFullYear(), startDate.getUTCMonth(), startDate.getUTCDate(), startDate.getUTCHours(), startDate.getUTCMinutes(), startDate.getUTCSeconds());
                    var endDate = new Date(item.EndDate);
                    copyItem.EndDate = new Date(endDate.getUTCFullYear(), endDate.getUTCMonth(), endDate.getUTCDate(), endDate.getUTCHours(), endDate.getUTCMinutes(), endDate.getUTCSeconds());
                    return copyItem;
                }
            }
        });
        
        $scope.modalInstance.result.then(function (item) {
            $scope.filter.range.maxCalculated = false;

            var gridElem = $scope.savingsDepositItems[$scope.modItemId];
            gridElem.Modified = item.Modified;
            gridElem.ModifiedById = item.ModifiedById;
            gridElem.ModifiedByName = item.ModifiedByName;
            gridElem.BankName = item.BankName;
            gridElem.AccountNumber = item.AccountNumber;
            gridElem.InitialAmount = item.InitialAmount;
            gridElem.StartDate = item.StartDate;
            gridElem.EndDate = item.EndDate;
            gridElem.InterestPercentage = item.InterestPercentage;
            gridElem.TaxesPercentage = item.TaxesPercentage;
            gridElem.Amount = item.Amount;
            gridElem.AmountWithTaxes = item.AmountWithTaxes;
        },
        function () {
        });
    }
});

savingsDepositManagerApp.controller('manageSavingsDepositManagerController', function ($scope, $uibModalInstance, SavingsDepositFactory, DataContent) {
    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };
    $scope.endDateOptions = {
        formatYear: 'yy',
        startingDay: 1
    }
    $scope.format = 'MM/dd/yyyy';
    $scope.altInputFormats = ['M!/d!/yyyy'];

    $scope.startDateSelector = {
        opened: false
    };
    $scope.endDateSelector = {
        opened: false
    };

    $scope.getUTCDate = function (date) {
        try{
            return new Date(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(),  0, 0, 0);
        }
        catch (err) { }
    }
    $scope.dateInvalid = false;
    $scope.validateMinDate = function () {
        if ($scope.getUTCDate($scope.dataContent.EndDate) < $scope.getUTCDate($scope.dataContent.StartDate)) {
            $scope.dateInvalid = true;
        }
        else {
            $scope.dateInvalid = false;
        }
    }

    $scope.dataContent = DataContent;
    $scope.modalTitle = 'Add New Savings Deposit';
    if ($scope.dataContent.Id != 0) {
        $scope.modalTitle = 'Edit Savings Deposit';
    }

    $scope.closeModalClick = function () {
        $uibModalInstance.dismiss('cancel');
    }

    $scope.save = function () {
        if ($scope.dataContent.Id == 0) {
            SavingsDepositFactory.save($scope.dataContent, function (result) {
                $uibModalInstance.close(result);
            },
                function (error) {
                    alertService.showError("Update Office", error);
                    alert(error);
                }
            );
        }
        else {
            SavingsDepositFactory.update({ id: $scope.dataContent.Id }, $scope.dataContent,
                function (result) {
                    $uibModalInstance.close(result);
                },
                function (error) {
                    alertService.showError("Update Office", error);
                    alert(error);
                }
            );
        }
    }
});