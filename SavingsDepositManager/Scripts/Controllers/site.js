﻿var mainApp = angular.module('mainApp', [
    'ngResource',
    'ui.bootstrap',
    'ui.bootstrap.modal',
    'ui.bootstrap.datepicker',
    'ui.bootstrap.tpls',
    'ngTable',
    'ui-rangeSlider',
    'blockUI',
    'ngRoute',
    'ngStorage',
    'cgNotify',
    'factoriesApp',
    'directivesApp',
    'filterApp',
    'savingsDepositManagerApp',
    'manageUsersApp',
    'reportApp',
    "ngAnimate"
]);


mainApp.config(['$routeProvider', '$locationProvider', '$httpProvider', function ($routeProvider, $locationProvider, $httpProvider) {
    
    $routeProvider
        .when('/home',
            {
                templateUrl: 'Templates/Pages/main.html',
                controller: 'appStartController'
            })
        .when('/managedeposits',
            {
                templateUrl: 'Templates/Pages/savingsDeposit.html',
                controller: 'savingsDepositManagerController'
            })
        .when('/login',
            {
                templateUrl: 'Templates/Pages/login.html',
                controller: 'appStartController'
            })
        .when('/register',
            {
                templateUrl: 'Templates/Pages/register.html',
                controller: 'appStartController'
            })
        .when('/manageusers',
            {
                templateUrl: 'Templates/Pages/manageusers.html',
                controller: 'manageUsersController'
            })
        .when('/report',
            {
                templateUrl: 'Templates/Pages/report.html',
                controller: 'reportController'
            })
        .otherwise(
            {
                redirectTo: '/home'
            });

    $locationProvider.html5Mode({
        enabled: true,
        requireBase: false
    });

    $httpProvider.interceptors.push(function ($q, $location, $localStorage, $injector, blockUI) {
        return {
            'request': function (config) {
                config.headers = config.headers || {};
                if ($localStorage.tokenData) {
                    config.headers.Authorization = 'Bearer ' + $localStorage.tokenData.access_token;
                }
                return config;
            },
            'responseError': function (response) {

                blockUI.stop();

                if (response.status === 401 || response.status === 403) {
                    var src = $location.$$path;
                    $location.path("login").search({ source: src });
                }
                return $q.reject(response);
            }
        };
    });
}]);

mainApp.controller('appStartController', function ($scope, $rootScope, $uibModal, $localStorage, $sessionStorage, $location, blockUI, notify, LoginFactory) {

    $scope.$storage = $localStorage;

    $rootScope.isAuthenticated = false;
    $rootScope.isAdmin = false;
    $rootScope.isAdminOrManager = false;
    

    $scope.$watch(function (scope) { return $scope.$storage.tokenData },
              function (newValue, oldValue) {
                  if (newValue && new Date(newValue.expires) > new Date()) {
                      $rootScope.isAuthenticated = true;
                      var roles = newValue.roles.split(',');
                      for (var i = 0; i < roles.length; i++) {
                          if (roles[i] == 'Admin' ||
                              roles[i] == 'Manager') {
                              $rootScope.isAdminOrManager = true;
                          }

                          if (roles[i] == 'Admin') {
                              $rootScope.isAdmin = true;
                          }
                      }
                  }
                  else {
                      $rootScope.isAuthenticated = false;
                      $rootScope.isAdmin = false;
                      $rootScope.isAdminOrManager = false;
                  }
              }
             );

    $scope.loginData = {
        grant_type: 'password',
        username: '',
        password: ''
    };
    $scope.registerData = {
        Email: '',
        Password: '',
        ConfirmPassword: ''
    };

    $scope.login = function () {

        LoginFactory.login(
            'grant_type=password&username=' + $scope.loginData.username + '&password=' + $scope.loginData.password,
            function (result) {

                notify({
                    message: 'You successfully authenticated.',
                    position: 'right',
                    classes: 'notify-success',
                    duration: 3000
                });

                $scope.$storage.tokenData = {
                    expires: new Date(result[".expires"]),
                    access_token: result.access_token,
                    userName: result.userName,
                    roles: result.roles
                };

                var source = $location.search().source;
                if (!source) {
                    source = "/home";
                }

                $location.path(source).search({});
            },
            function (error) {
                notify({
                    message: error.data.error_description,
                    position: 'right',
                    classes: 'notify-danger',
                    duration: 5000
                });
            }
        );
    }

    $scope.register = function () {

        LoginFactory.register(
            'Email=' + $scope.registerData.Email + '&Password=' + $scope.registerData.Password + '&ConfirmPassword=' + $scope.registerData.ConfirmPassword,
            function (result) {

                notify({
                    message: 'You successfully registered.',
                    position: 'right',
                    classes: 'notify-success',
                    duration: 3000
                });

                $scope.loginData.username = $scope.registerData.Email;
                $scope.loginData.password = $scope.registerData.Password;
                $scope.login();
            },
            function (error) {
                notify({
                    message: error.data.ModelState[""][0],
                    position: 'right',
                    classes: 'notify-danger',
                    duration: 5000
                });
            }
        );
    }

    $scope.logout = function () {

        LoginFactory.logout(
            '',
            function (result) {
                
                notify({
                    message: 'You successfully logged out.',
                    position: 'right',
                    classes: 'notify-success',
                    duration: 3000
                });

                delete $scope.$storage.tokenData;

                $location.path('home').search({});
            },
            function (error) {
                notify({
                    message: error.data.error_description,
                    position: 'right',
                    classes: 'notify-danger',
                    duration: 5000
                });
            }
        );
    }
});