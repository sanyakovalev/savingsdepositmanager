﻿var factoriesApp = angular.module('factoriesApp', ['ngResource']);

factoriesApp.factory('SavingsDepositFactory', ['$resource',
    function ($resource) {
        return $resource('/api/SavingsDepositItems/:id', {},
            {
                'update': { method: 'PUT' },
                'queryFiltered': { method: 'GET', isArray: false },
                'report': { method: 'GET', isArray: false, url: '/api/SavingsDepositItems/report' }
            }
        );
    }]
);

factoriesApp.factory('ManageUsersFactory', ['$resource',
    function ($resource) {
        return $resource('/api/ManageUsers/:id', {},
            {
                'update': { method: 'PUT' },
                'getUsers': {
                    method: 'GET',
                    isArray: false
                },
            }
        );
    }]
);

factoriesApp.factory('LoginFactory', ['$resource',
    function ($resource) {
        return $resource('/api/Account/:id', {},
            {
                'login': {
                    method: 'POST', isArray: false, url: '/Token',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }
                },
                'register': {
                    method: 'POST', isArray: false, url: '/api/Account/Register',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }
                },
                'logout': {
                    method: 'POST', isArray: false, url: '/api/Account/Logout'
                }
            }
        );
    }]
);