﻿var filterApp = angular.module('filterApp', []);

filterApp.filter('yesNo', function () {
    return function (input) {
        return input ? 'Yes' : 'No';
    }
});
