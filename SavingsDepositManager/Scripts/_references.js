/// <autosync enabled="true" />
/// <reference path="angular.min.js" />
/// <reference path="angular.rangeslider.js" />
/// <reference path="angular-animate.min.js" />
/// <reference path="angular-aria.min.js" />
/// <reference path="angular-block-ui.min.js" />
/// <reference path="angular-cookies.min.js" />
/// <reference path="angular-loader.min.js" />
/// <reference path="angular-message-format.min.js" />
/// <reference path="angular-messages.min.js" />
/// <reference path="angular-mocks.js" />
/// <reference path="angular-parse-ext.min.js" />
/// <reference path="angular-resource.min.js" />
/// <reference path="angular-route.min.js" />
/// <reference path="angular-sanitize.min.js" />
/// <reference path="angular-scenario.js" />
/// <reference path="angular-touch.min.js" />
/// <reference path="angular-ui/ui-bootstrap.min.js" />
/// <reference path="angular-ui/ui-bootstrap-tpls.min.js" />
/// <reference path="bootstrap.min.js" />
/// <reference path="controllers/manageusers.js" />
/// <reference path="controllers/report.js" />
/// <reference path="controllers/savingsdepositmanager.js" />
/// <reference path="controllers/site.js" />
/// <reference path="directives/directives.js" />
/// <reference path="factories/factories.js" />
/// <reference path="filters/filters.js" />
/// <reference path="jquery.validate.min.js" />
/// <reference path="jquery.validate.unobtrusive.js" />
/// <reference path="jquery-3.1.1.min.js" />
/// <reference path="jquery-3.1.1.slim.min.js" />
/// <reference path="modernizr-2.8.3.js" />
/// <reference path="ngstorage.min.js" />
/// <reference path="ng-table.min.js" />
/// <reference path="notify/angular-notify.js" />
/// <reference path="respond.matchmedia.addlistener.min.js" />
/// <reference path="respond.min.js" />
